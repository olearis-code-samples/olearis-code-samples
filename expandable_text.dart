import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

class ExpandableText extends StatefulWidget {
  final String text;
  final TextStyle style;
  final int maxLinesCountInCollapsedState;
  final String readMoreLinkText;
  final String readLessLinkText;
  final String trimmedTextIndicator;
  final TextStyle linksStyle;
  final TextDirection textDirection;
  final Duration animationDuration;
  final Curve animationCurve;

  @override
  ExpandableTextState createState() => ExpandableTextState();

  ExpandableText({
    required this.text,
    required this.style,
    required this.maxLinesCountInCollapsedState,
    required this.readMoreLinkText,
    required this.readLessLinkText,
    required this.linksStyle,
    this.trimmedTextIndicator = "...",
    this.textDirection = TextDirection.ltr,
    this.animationDuration = const Duration(milliseconds: 150),
    this.animationCurve = Curves.easeInOut,
    super.key,
  });
}

class ExpandableTextState extends State<ExpandableText> {
  bool _isCollapsed = true;

  late String _originalText;
  late TextStyle _originalTextStyle;

  late TapGestureRecognizer _switchCollapsedStateGestureRecognizer;

  @override
  void initState() {
    // original properties
    _originalText = widget.text;
    _originalTextStyle = widget.style;

    // build "expand/collapse" gesture recognizer
    _switchCollapsedStateGestureRecognizer = TapGestureRecognizer()..onTap = _switchCollapsedState;

    // init super
    super.initState();
  }

  void _switchCollapsedState() {
    setState(() {
      _isCollapsed = !_isCollapsed;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedSize(
      alignment: Alignment.topCenter,
      curve: widget.animationCurve,
      duration: widget.animationDuration,
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          assert(constraints.hasBoundedWidth);
          return _ExpandableTextBody(
            minWidth: constraints.minWidth,
            maxWidth: constraints.maxWidth,
            isCollapsed: _isCollapsed,
            switchCollapsedStateGestureRecognizer: _switchCollapsedStateGestureRecognizer,
            originalText: _originalText,
            originalTextStyle: _originalTextStyle,
            maxLinesCountInCollapsedState: widget.maxLinesCountInCollapsedState,
            readMoreLinkText: widget.readMoreLinkText,
            readLessLinkText: widget.readLessLinkText,
            linksStyle: widget.linksStyle,
            trimmedTextIndicator: widget.trimmedTextIndicator,
            textDirection: widget.textDirection,
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _switchCollapsedStateGestureRecognizer.dispose();
    super.dispose();
  }
}

class _ExpandableTextBody extends StatelessWidget {
  final double minWidth;
  final double maxWidth;
  final bool isCollapsed;
  final TapGestureRecognizer switchCollapsedStateGestureRecognizer;

  final String originalText;
  final TextStyle originalTextStyle;
  final int maxLinesCountInCollapsedState;
  final String readMoreLinkText;
  final String readLessLinkText;
  final String trimmedTextIndicator;
  final TextStyle linksStyle;
  final TextDirection textDirection;

  const _ExpandableTextBody({
    required this.minWidth,
    required this.maxWidth,
    required this.isCollapsed,
    required this.switchCollapsedStateGestureRecognizer,
    required this.originalText,
    required this.originalTextStyle,
    required this.maxLinesCountInCollapsedState,
    required this.readMoreLinkText,
    required this.readLessLinkText,
    required this.linksStyle,
    required this.trimmedTextIndicator,
    required this.textDirection,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // create originalText painter
    final originalTextSpan = TextSpan(text: originalText, style: originalTextStyle);
    final originalTextPainter = TextPainter(
      text: originalTextSpan,
      textDirection: textDirection,
      maxLines: maxLinesCountInCollapsedState,
      ellipsis: trimmedTextIndicator,
    );

    // layout original text with given width to find height properties
    originalTextPainter.layout(minWidth: minWidth, maxWidth: maxWidth);

    // check if text is large -> its lines count is more than allowed
    final isTextLarge = originalTextPainter.didExceedMaxLines;

    // build content TextSpan according to isTextLarge or not.
    final TextSpan contentTextSpan;
    if (isTextLarge) {
      if (isCollapsed) {
        // state is "trimmed lines + ... + " " + read more"

        // build all "adding" span = "..." + " " + "read more"
        final addingSpan = _buildAddingToTheEndReadMoreSpan();

        // calculate size of all originalText
        final originalTextSize = originalTextPainter.size;

        // calculate adding span size
        final addingSpanPainter = TextPainter(text: addingSpan, textDirection: textDirection);
        addingSpanPainter.layout(minWidth: minWidth, maxWidth: maxWidth);
        final addingSpanSize = addingSpanPainter.size;

        // state is going to be ["trimmed lines + ... + " " + read more"]
        // So let's find the position "end index of displaying original text" -> it is before first "..."
        final textPositionOfEndingDisplayedOriginalText = originalTextPainter.getPositionForOffset(
          // we use here originalTextSize.height and it is the height of "displayed" part ->
          // because this is size returned from painter and painter has max lines count.
          Offset(originalTextSize.width - addingSpanSize.width, originalTextSize.height),
        );
        final textIndexOfEndingDisplayedOriginalText =
            originalTextPainter.getOffsetBefore(textPositionOfEndingDisplayedOriginalText.offset);
        if (textIndexOfEndingDisplayedOriginalText != null) {
          final trimmedOriginalText = originalText.substring(0, textIndexOfEndingDisplayedOriginalText);
          contentTextSpan = TextSpan(text: trimmedOriginalText, style: originalTextStyle, children: [addingSpan]);
        } else {
          // if something went wrong -> just show full original text.
          contentTextSpan = originalTextSpan;
        }
      } else {
        // state is "all lines + " " + read less"
        contentTextSpan = TextSpan(
          text: originalText,
          style: originalTextStyle,
          children: [
            _buildAddingToTheEndReadLessSpan(),
          ],
        );
      }
    } else {
      // text is not too large -> just show full original span.
      contentTextSpan = originalTextSpan;
    }

    return Text.rich(contentTextSpan, softWrap: true, overflow: TextOverflow.clip);
  }

  TextSpan _buildAddingToTheEndReadMoreSpan() {
    // just "read more" link with gesture
    final readMoreLink = TextSpan(
      text: readMoreLinkText,
      style: linksStyle,
      recognizer: switchCollapsedStateGestureRecognizer,
    );

    // all span = "..." + " " + "read more"
    final addingSpan = TextSpan(text: "$trimmedTextIndicator ", style: originalTextStyle, children: [readMoreLink]);
    return addingSpan;
  }

  TextSpan _buildAddingToTheEndReadLessSpan() {
    // just "read less" link with gesture
    final readLessLink = TextSpan(
      text: readLessLinkText,
      style: linksStyle,
      recognizer: switchCollapsedStateGestureRecognizer,
    );

    // all span = " " + "read less"
    final addingSpan = TextSpan(text: " ", style: originalTextStyle, children: [readLessLink]);
    return addingSpan;
  }
}
